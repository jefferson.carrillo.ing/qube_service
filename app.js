const express = require("express");
const bodyParser = require("body-parser");
const Cors = require("./utils/Cors");
const config = require("./config/Config");
const ip = require("ip").address();
const configure_exe = config[config.ejecutar]; //desarrollo | produccion

app = express();
app.use(Cors.GlobalCors);
app.options("*", Cors.GlobalCors);
app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
app.use('/static', express.static(__dirname + '/public'));

app.pro = { configure_exe, config };



const Utilitys = require("./utils/Utilitys");
app.pro.Utilitys = Utilitys;

var ContactRoute = require("./API/Contact/routers/ContactRoute");
app.use("/", ContactRoute);

var QuestionRoute = require("./API/Question/routers/QuestionRoute");
app.use("/", QuestionRoute);

var RenderRoute = require("./API/Render/routers/RenderRoute");
app.use("/", RenderRoute);

var SystemRoute = require("./API/System/routers/SystemRoute");
app.use("/", SystemRoute);

var LanguageRoute = require("./API/Language/routers/LanguageRoute");
app.use("/", LanguageRoute);

//Utilitys.InicializarBaseDatos({
// callback: async function (params) {
//ejecutar despues de inicializar base de datos

// Puerto de escucha del servidor
let CHTTP = configure_exe.local.CONNECTION_HTTP;
let port = process.env.PORT || CHTTP.puerto;
app.listen(port, function () {
  Utilitys.ConsoleTabInfo({
    //mensaje en consola
    title: "CONFIGURACIONES " + config.name_proyect,
    color: "blue",
    break: true, //rompe regla de consola
    childre: async function () {
      Utilitys.ConsoleInfo({
        text: "Type of configuration: " + config.ejecutar,
        color: "green",
      });
      Utilitys.ConsoleInfo({
        text: "listen: http://localhost:" + port,
        color: "green",
      });
      Utilitys.ConsoleInfo({
        text: "listen: http://" + ip + ":" + port,
        color: "magenta",
      });
    },
  });
});
// }
//});



