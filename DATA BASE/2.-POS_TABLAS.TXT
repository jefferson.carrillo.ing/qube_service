CREATE TABLE "category" (
"id" serial4 NOT NULL,
"code" varchar(50),
"name" varchar(200),
"description" varchar(500),
"type" int4,
"condition" int4,
"created_at" timestamp(0),
"updated_at" timestamp(0),
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON COLUMN "category"."code" IS 'codigo para seguimiento';
COMMENT ON COLUMN "category"."name" IS 'nombre de registro';
COMMENT ON COLUMN "category"."description" IS 'descripcion de registro';
COMMENT ON COLUMN "category"."type" IS 'sirve para dar logica en que modulo usarlo';
COMMENT ON COLUMN "category"."condition" IS 'estado del rgistro 1 = activo, 0 = desactivado';
COMMENT ON COLUMN "category"."created_at" IS 'fecha de creacion del registro';
COMMENT ON COLUMN "category"."updated_at" IS 'ultima fecha de actualizacion del registro';

CREATE TABLE "propietario" (
"id" serial4 NOT NULL,
"nombre" varchar(200),
"descripcion" varchar(500),
"estado" int4,
"created_at" timestamp(0),
"updated_at" timestamp(0),
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
CREATE TABLE "owner" (
"id" serial4 NOT NULL,
"code" varchar(50),
"name" varchar(200),
"description" varchar(500),
"type" int4,
"condition" int4,
"created_at" timestamp(0),
"updated_at" timestamp(0),
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON COLUMN "owner"."code" IS 'codigo para seguimiento';
COMMENT ON COLUMN "owner"."name" IS 'nombre de registro';
COMMENT ON COLUMN "owner"."description" IS 'descripcion de registro';
COMMENT ON COLUMN "owner"."type" IS 'sirve para dar logica en que modulo usarlo';
COMMENT ON COLUMN "owner"."condition" IS 'estado del rgistro 1 = activo, 0 = desactivado';
COMMENT ON COLUMN "owner"."created_at" IS 'fecha de creacion del registro';
COMMENT ON COLUMN "owner"."updated_at" IS 'ultima fecha de actualizacion del registro';

CREATE TABLE "country" (
"id" serial4 NOT NULL,
"code" varchar(50),
"name" varchar(200),
"description" varchar(500),
"condition" int4,
"created_at" timestamp(0),
"updated_at" timestamp(0),
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON COLUMN "country"."code" IS 'codigo para seguimiento';
COMMENT ON COLUMN "country"."name" IS 'nombre de registro';
COMMENT ON COLUMN "country"."description" IS 'descripcion de registro';
COMMENT ON COLUMN "country"."condition" IS 'estado del rgistro 1 = activo, 0 = desactivado';
COMMENT ON COLUMN "country"."created_at" IS 'fecha de creacion del registro';
COMMENT ON COLUMN "country"."updated_at" IS 'ultima fecha de actualizacion del registro';

CREATE TABLE "province" (
"id" serial4 NOT NULL,
"id_country" int4,
"code" varchar(50),
"name" varchar(200),
"description" varchar(500),
"condition" int4,
"created_at" timestamp(0),
"updated_at" timestamp(0),
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON COLUMN "province"."code" IS 'codigo para seguimiento';
COMMENT ON COLUMN "province"."name" IS 'nombre de registro';
COMMENT ON COLUMN "province"."description" IS 'descripcion de registro';
COMMENT ON COLUMN "province"."condition" IS 'estado del rgistro 1 = activo, 0 = desactivado';
COMMENT ON COLUMN "province"."created_at" IS 'fecha de creacion del registro';
COMMENT ON COLUMN "province"."updated_at" IS 'ultima fecha de actualizacion del registro';

CREATE TABLE "canton" (
"id" serial4 NOT NULL,
"id_province" int4,
"code" varchar(50),
"name" varchar(200),
"description" varchar(500),
"condition" int4,
"created_at" timestamp(0),
"updated_at" timestamp(0),
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON COLUMN "canton"."code" IS 'codigo para seguimiento';
COMMENT ON COLUMN "canton"."name" IS 'nombre de registro';
COMMENT ON COLUMN "canton"."description" IS 'descripcion de registro';
COMMENT ON COLUMN "canton"."condition" IS 'estado del rgistro 1 = activo, 0 = desactivado';
COMMENT ON COLUMN "canton"."created_at" IS 'fecha de creacion del registro';
COMMENT ON COLUMN "canton"."updated_at" IS 'ultima fecha de actualizacion del registro';

CREATE TABLE "publication" (
"id" serial4 NOT NULL,
"id_canton" int4,
"id_owner" int4,
"id_type_money" int4,
"code" varchar(50),
"name" varchar(200),
"commentary" varchar(500),
"
latitude" varchar(500),
"longitude" varchar(800),
"condition" int4,
"created_at" timestamp(0),
"updated_at" timestamp(0),
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON COLUMN "publication"."code" IS 'codigo para seguimiento';
COMMENT ON COLUMN "publication"."name" IS 'nombre de registro';
COMMENT ON COLUMN "publication"."commentary" IS 'descripcion de registro';
COMMENT ON COLUMN "publication"."condition" IS 'estado del rgistro 1 = activo, 0 = desactivado';
COMMENT ON COLUMN "publication"."created_at" IS 'fecha de creacion del registro';
COMMENT ON COLUMN "publication"."updated_at" IS 'ultima fecha de actualizacion del registro';

CREATE TABLE "publication_category" (
"id" serial4 NOT NULL,
"id_publication" int4,
"id_category" int4,
"condition" int4,
"created_at" timestamp(0),
"updated_at" timestamp(0),
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON COLUMN "publication_category"."condition" IS 'estado del rgistro 1 = activo, 0 = desactivado';
COMMENT ON COLUMN "publication_category"."created_at" IS 'fecha de creacion del registro';
COMMENT ON COLUMN "publication_category"."updated_at" IS 'ultima fecha de actualizacion del registro';

CREATE TABLE "files" (
"id" serial4 NOT NULL,
"id_publication" int4,
"name" varchar(200),
"description" varchar(500),
"type" int4,
"condition" int4,
"created_at" timestamp(0),
"updated_at" timestamp(0),
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON COLUMN "files"."name" IS 'nombre de registro';
COMMENT ON COLUMN "files"."description" IS 'descripcion de registro';
COMMENT ON COLUMN "files"."type" IS 'sirve para dar logica en que modulo usarlo';
COMMENT ON COLUMN "files"."condition" IS 'estado del rgistro 1 = activo, 0 = desactivado';
COMMENT ON COLUMN "files"."created_at" IS 'fecha de creacion del registro';
COMMENT ON COLUMN "files"."updated_at" IS 'ultima fecha de actualizacion del registro';

CREATE TABLE "type_money" (
"id" serial4 NOT NULL,
"code" varchar(50),
"name" varchar(200),
"description" varchar(500),
"type" int4,
"condition" int4,
"created_at" timestamp(0),
"updated_at" timestamp(0),
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON COLUMN "type_money"."code" IS 'codigo para seguimiento';
COMMENT ON COLUMN "type_money"."name" IS 'nombre de registro';
COMMENT ON COLUMN "type_money"."description" IS 'descripcion de registro';
COMMENT ON COLUMN "type_money"."type" IS 'sirve para dar logica en que modulo usarlo';
COMMENT ON COLUMN "type_money"."condition" IS 'estado del rgistro 1 = activo, 0 = desactivado';
COMMENT ON COLUMN "type_money"."created_at" IS 'fecha de creacion del registro';
COMMENT ON COLUMN "type_money"."updated_at" IS 'ultima fecha de actualizacion del registro';

CREATE TABLE "historical_price" (
"id" serial4 NOT NULL,
"id_publication" int4,
"price" decimal(30,4),
"description" varchar(500),
"type" int4,
"condition" int4,
"created_at" timestamp(0),
"updated_at" timestamp(0),
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON COLUMN "historical_price"."price" IS 'codigo para seguimiento';
COMMENT ON COLUMN "historical_price"."description" IS 'descripcion de registro';
COMMENT ON COLUMN "historical_price"."type" IS 'sirve para dar logica en que modulo usarlo';
COMMENT ON COLUMN "historical_price"."condition" IS 'estado del rgistro 1 = activo, 0 = desactivado';
COMMENT ON COLUMN "historical_price"."created_at" IS 'fecha de creacion del registro';
COMMENT ON COLUMN "historical_price"."updated_at" IS 'ultima fecha de actualizacion del registro';

CREATE TABLE "system" (
"id" serial4 NOT NULL,
"key" varchar(500),
"value" varchar(500),
"description" varchar(500),
"type" int4,
"condition" int4,
"created_at" timestamp(0),
"updated_at" timestamp(0),
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON COLUMN "system"."key" IS 'codigo para seguimiento';
COMMENT ON COLUMN "system"."value" IS 'nombre de registro';
COMMENT ON COLUMN "system"."description" IS 'descripcion de registro';
COMMENT ON COLUMN "system"."type" IS 'sirve para dar logica en que modulo usarlo';
COMMENT ON COLUMN "system"."condition" IS 'estado del rgistro 1 = activo, 0 = desactivado';
COMMENT ON COLUMN "system"."created_at" IS 'fecha de creacion del registro';
COMMENT ON COLUMN "system"."updated_at" IS 'ultima fecha de actualizacion del registro';

CREATE TABLE "questions" (
"id" serial4 NOT NULL,
"description" varchar,
"idioma" int4 DEFAULT 1,
"type" int4,
"condition" int4,
"created_at" timestamp(0),
"updated_at" timestamp(0),
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON COLUMN "questions"."description" IS 'descripcion de registro';
COMMENT ON COLUMN "questions"."idioma" IS '1 ingles, 2 español';
COMMENT ON COLUMN "questions"."type" IS 'sirve para dar logica en que modulo usarlo';
COMMENT ON COLUMN "questions"."condition" IS 'estado del rgistro 1 = activo, 0 = desactivado';
COMMENT ON COLUMN "questions"."created_at" IS 'fecha de creacion del registro';
COMMENT ON COLUMN "questions"."updated_at" IS 'ultima fecha de actualizacion del registro';

CREATE TABLE "answers" (
"id" serial4 NOT NULL,
"id_questions" int4,
"description" varchar,
"idioma" int4 DEFAULT 1,
"type" int4,
"condition" int4,
"created_at" timestamp(0),
"updated_at" timestamp(0),
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON COLUMN "answers"."description" IS 'respuesta de pregunta';
COMMENT ON COLUMN "answers"."idioma" IS '1 ingles, 2 español';
COMMENT ON COLUMN "answers"."type" IS 'sirve para dar logica  al registro';
COMMENT ON COLUMN "answers"."condition" IS 'estado del rgistro 1 = activo, 0 = desactivado';
COMMENT ON COLUMN "answers"."created_at" IS 'fecha de creacion del registro';
COMMENT ON COLUMN "answers"."updated_at" IS 'ultima fecha de actualizacion del registro';

CREATE TABLE "contact" (
"id" serial4 NOT NULL,
"email" varchar,
"description" varchar,
"idioma" int4 DEFAULT 1,
"type" int4,
"condition" int4,
"created_at" timestamp(0),
"updated_at" timestamp(0),
PRIMARY KEY ("id") 
)
WITHOUT OIDS;
COMMENT ON COLUMN "contact"."description" IS 'descripcion de registro';
COMMENT ON COLUMN "contact"."idioma" IS '1 ingles, 2 español';
COMMENT ON COLUMN "contact"."type" IS 'sirve para dar logica en que modulo usarlo';
COMMENT ON COLUMN "contact"."condition" IS 'estado del rgistro 1 = activo, 0 = desactivado';
COMMENT ON COLUMN "contact"."created_at" IS 'fecha de creacion del registro';
COMMENT ON COLUMN "contact"."updated_at" IS 'ultima fecha de actualizacion del registro';


ALTER TABLE "province" ADD CONSTRAINT "fk_province_country_1" FOREIGN KEY ("id_country") REFERENCES "country" ("id");
ALTER TABLE "canton" ADD CONSTRAINT "fk_canton_province_1" FOREIGN KEY ("id_province") REFERENCES "province" ("id");
ALTER TABLE "publication" ADD CONSTRAINT "fk_publication_canton_1" FOREIGN KEY ("id_canton") REFERENCES "canton" ("id");
ALTER TABLE "publication" ADD CONSTRAINT "fk_publication_owner_1" FOREIGN KEY ("id_owner") REFERENCES "owner" ("id");
ALTER TABLE "publication_category" ADD CONSTRAINT "fk_publication_category_category_1" FOREIGN KEY ("id_category") REFERENCES "category" ("id");
ALTER TABLE "publication_category" ADD CONSTRAINT "fk_publication_category_publication_1" FOREIGN KEY ("id_publication") REFERENCES "publication" ("id");
ALTER TABLE "files" ADD CONSTRAINT "fk_images_publication_1" FOREIGN KEY ("id_publication") REFERENCES "publication" ("id");
ALTER TABLE "publication" ADD CONSTRAINT "fk_publication_type_money_1" FOREIGN KEY ("id_type_money") REFERENCES "type_money" ("id");
ALTER TABLE "historical_price" ADD CONSTRAINT "fk_historical_price_publication_1" FOREIGN KEY ("id_publication") REFERENCES "publication" ("id");
ALTER TABLE "answers" ADD CONSTRAINT "fk_answers_questions_1" FOREIGN KEY ("id_questions") REFERENCES "questions" ("id");

