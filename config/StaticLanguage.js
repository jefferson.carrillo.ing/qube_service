//MENSAJES STATICOS CONFIGURADOS :
//United_Kingdom
//Spain

var ListLanguage = {
  United_Kingdom: {
    global: {
      error: "An unexpected error has occurred.",
      received: "The process has been carried out correctly.",
      not_content : "There is no content, it is recommended to add it."
    },
    contact: {
      received: "We have received your information, we will contact you soon.",
    },
    language: {
      save: "A new language was created successfully.",
    },
    render: {
      save: "A new render component was created successfully.",
      show: "The following information was found.",
      update: "Render component updated successfully.",
    },
    system: {
      save: "A new system component was created successfully.",
      show: "The following information was found.",
      update: "System component updated successfully.",
    },
    question: {
      received:
        "We have received your question and will answer it as soon as we can.",
    }
  },
  Spain: {
    global: {
      error: "Ha ocurrido un error inesperado.",
      received: "se ha realizado el proceso de manera correcta.",
      not_content : "No existe contenido, se recomienda agregarlo."
    },
    contact: {
      received:
        "Hemos recibido tu información, te contactaremos a la brevedad.",
    },
    language: {
      save: "Se creo un nuevo idioma exitosamente.",
    },
    render: {
      save: "Se creo un nuevo componente render exitosamente.",
      show: "Se encontro la siguiente información .",
      update: "Se actualizo con exito el componente de renderizado.",
    },
    system: {
      save: "Se creo un nuevo componente system exitosamente.",
      show: "Se encontro la siguiente información .",
      update: "Se actualizo con exito el componente sistema.",
    },
    question: {
      received:
        "Hemos recibido tu pregunta y la responderemos en cuanto podamos.",
    },

  },
};

module.exports = ListLanguage;
