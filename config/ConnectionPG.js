
const configure_exe = app.pro.configure_exe;
//set set timezone='America/Guayaquil'; 
//show timezone

const Cadena = { 
    client: 'pg',
    connection: configure_exe.local.CONNECTION_BD_PG,
    searchPath: ['knex', 'public'],
    useNullAsDefault: true,// valores de vacios  con null al usar insert
    depuración: false,
    debug: false, 

};

//modifica string a decimales 
const pg = require('pg');
//const PG_DECIMAL_OID = 1700;
//pg.types.setTypeParser(PG_DECIMAL_OID, parseFloat);
const TIMESTAMPTZ_OID = 1184;
const TIMESTAMP_OID = 1114;
pg.types.setTypeParser(TIMESTAMPTZ_OID, val => val);
pg.types.setTypeParser(TIMESTAMP_OID, val => val);


const knex = require('knex')(Cadena);

function ModelORM(data) {
    let Tablas = data.name_tables; //ARRAY
    var Modelo = knex(Tablas);
    return Modelo;
}

function DefaultConexion() {
    //crear un usuario por defecto en postgres con una clave 
    const Cadena = {
        client: 'pg',
        connection:
        {
            host: configure_exe.local.CONNECTION_BD_PG.host,
            port: configure_exe.local.CONNECTION_BD_PG.port,
            user: configure_exe.local.CONNECTION_BD_PG_DEFAULT.user,
            password: configure_exe.local.CONNECTION_BD_PG_DEFAULT.password,
            database: 'postgres'
        },
        searchPath: ['knex', 'public'],
        useNullAsDefault: true // valores de vacios  con null al usar insert
    };
    var knex = require('knex')(Cadena);
    return knex;
}

//var bookshelf = require('bookshelf')(knex);

module.exports = {
    knex: knex,
    ModelORM: ModelORM,
    DefaultConexion: DefaultConexion
};

// PAGINA https://knexjs.org/