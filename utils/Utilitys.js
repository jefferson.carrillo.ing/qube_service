const Logs = require("./Logs");
const config = app.pro.config;
const configure_exe = app.pro.configure_exe;
const dateFormat = require("dateformat");
const ConnectionPG = require("../config/ConnectionPG");
const fs = require("fs");
const StaticLanguage = require("../config/StaticLanguage");

const ConsolColors = {
  reset: "\033[0m",
  //text color
  black: "\033[30m",
  red: "\033[31m",
  green: "\033[32m",
  yellow: "\033[33m",
  blue: "\033[34m",
  magenta: "\033[35m",
  cyan: "\033[36m",
  white: "\033[37m",
  //background color
  blackBg: "\033[40m",
  redBg: "\033[41m",
  greenBg: "\033[42m",
  yellowBg: "\033[43m",
  blueBg: "\033[44m",
  magentaBg: "\033[45m",
  cyanBg: "\033[46m",
  whiteBg: "\033[47m",
  // cierre de color
  Reset: "\x1b[0m",
};
let cont = "";

function ConsoleInfo(data) {
  let color = ConsolColors[data.color];
  let rest = ConsolColors.Reset;
  let text = data.text;
  console.log(color + text + rest);
}
function ConsoleInfoDebugger(req) {
  if (config.console_debugger) {
    let color = ConsolColors.cyan;
    let rest = ConsolColors.Reset;
    let cliente = req.headers["user-agent"];
    let ruta = req.originalUrl;
    let now = new Date();
    let fecha = dateFormat(now, "mm-dd-yyyy  h:MM:ss:l");
    let s = "______________________________________________";
    console.log(color + s);
    console.log("Date: " + fecha);
    console.log("Route: " + ruta);
    console.log("Client: " + cliente);
    console.log(s + rest);
  }
}
function ConsoleTabInfo(data) {
  let now = new Date();
  let fecha = dateFormat(now, "mm-dd-yyyy  h:MM:ss:l");
  // https://misc.flogisoft.com/bash/tip_colors_and_formatting
  if (config.console_info || data.break == true) {
    let color = ConsolColors[data.color];
    let rest = ConsolColors.Reset;

    let titulo = data.title;
    let grafi = "===================";
    console.log(color + cont + fecha + " [" + titulo + "]" + grafi + rest);
    cont = cont + " ";
    let O = data.obj;
    if (O != undefined) {
      let text = JSON.stringify(O);
      console.log(cont + text);
    }
    let C = data.childre;
    if (C != undefined) {
      data.childre(C);
      cont = "";
    }
    let t = titulo.length + 3 + fecha.length;
    for (let index = 0; index < t; index++) {
      grafi = grafi + "=";
    }
    console.log(color + cont + grafi + rest);
  }
}

async function promesa_global(Temp, callback) {
  let raw = Temp.sql;
  let method = "select";
  let table = "FUNCION";
  if (raw == undefined) {
    method = Temp._method;
    table = Temp._single.table;
    switch (method) {
      case "insert":
        break;
      case "update":
        Temp._single.update.updated_at = await GetFechaCentral(); //actualizar fecha de cambios
        if (Temp._single.update.eliminar != undefined) {
          //para usar eliminado logico
          method = "delete-logit";
          delete Temp._single.update["eliminar"];
        }
        break;
    }
  }

  let msj_oper = method + " in " + table.toString();
  let log = { id_user: 0, method: method, table: table };
  let status = 500;
  let msj =
    "La solicitud del navegador no se ha podido completar porque se ha producido un error inesperado en el servidor";

  if (Temp.trx != undefined) {
    Temp = Temp.transacting(Temp.trx);
  }

  return Temp.then(function (obj) {
    obj.validacion = "";
    if (raw != undefined) {
      obj = obj.rows;
    }

    if (obj.length == 0 || (obj.length == undefined && obj < 0)) {
      status = 220; // no devuelve contenido 204
      msj = "No se encontraron registros";
    } else {
      switch (method) {
        case "select":
          status = 200;
          msj = "ok";
          break;
        case "insert":
          status = 201;
          msj = "registro creado";
          break;
        case "update":
          status = 221;
          msj = "registro actualizado";
          break;
        case "delete-logit":
          status = 222;
          msj = "registro eliminado logicamente";
          break;
        case "del":
          status = 222;
          msj = "registro eliminado";
          break;
        default:
          break;
      }
    }

    if (Temp.trx != undefined) {
      Temp.trx.commit;
    }
    return callback({ status: status, auth: true, msj: msj, data: obj });
  }).catch(function (err) {
    if (configure_exe.crear_log) {
      logger = Logs.CreateLogFile({ name_file: table });
      log.err = err;
      logger.error(log, "err " + msj_oper);
    }
    if (Temp.trx != undefined) {
      Temp.trx.rollback;
    }
    return callback({ status: status, auth: true, msj: msj, err: err });
  });
}

async function InicializarBaseDatos(data) {
  //CONFIGURACION
  let raiz = "./";
  let callback = data.callback;

  //CRAER BASE DE DATOS
  let name_bd = configure_exe.local.CONNECTION_BD_PG.database;
  let sql_create = ' CREATE DATABASE  "' + name_bd + '" ';
  sql_create += " WITH OWNER = postgres ";
  sql_create += " ENCODING = 'UTF8' ";
  sql_create += " TABLESPACE = pg_default ";
  sql_create += " LC_COLLATE = 'C' ";
  sql_create += " LC_CTYPE = 'C' ";
  sql_create += " CONNECTION LIMIT = -1 ";
  sql_create += " template template0 ;";

  let Temp = ConnectionPG.DefaultConexion().raw(sql_create); //CREAMOS LA BASE DE DATOS
  let obj = await promesa_global(Temp, function (obj) {
    return obj;
  });
  let error = obj.err;
  if (error == undefined && obj.status != 500) {
    let msj1 = ("BASE DE DATOS :" + name_bd).padEnd(50, " ");

    console.log(msj1 + "  [CREADA]");
    //LEER SQL

    let ruta = raiz + "DATA BASE/";
    let archivos_sql = [
      //estructura
      "1.-CONFIGURACION.TXT",
      "2.-POS_TABLAS.TXT",
    ];

    let sql_error = [];
    for (let index = 0; index < archivos_sql.length; index++) {
      const sql = archivos_sql[index];
      let archivo = ruta + sql;

      if (fs.existsSync(archivo)) {
        archivo = fs.readFileSync(archivo, "utf-8");
        let Temp1 = ConnectionPG.knex.raw(archivo); //EJECUTAR QUERY
        archivo = ("SCRIPT: " + sql).padEnd(40, " ");
        await Temp1.then(async function (obj) {
          let text = archivo + " [EJECUTADO]";
          ConsoleInfo({
            text: text,
            color: "green",
          });
        }).catch(async function (obj) {
          let text = archivo + "codigo:" + obj.code + ", linea:" + obj.line;
          let resumen = archivo + "-> erorre en linea: " + obj.line;
          sql_error.push(resumen);
          ConsoleInfo({
            text: text,
            color: "red",
          });
        });
      } else {
        console.log("El archivo NO EXISTE!");
      }
    }

    if (sql_error.length == 0) {
      setTimeout(() => {
        callback({});
      }, 1000);
    } else {
      ConsoleTabInfo({
        //mensaje en consola
        title: "SE IDENTIFICARON LOS SIGIENTES ERRORES SQL",
        color: "yellow",
        break: true, //rompe regla de consola
        childre: function () {
          for (let index = 0; index < sql_error.length; index++) {
            const el = sql_error[index];
            ConsoleInfo({
              text: el,
              color: "red",
            });
          }
        },
      });
    }
  } else if (error.code == "42P04") {
    callback({});
  } else {
    ConsoleTabInfo({
      //mensaje en consola
      title: "ERROR AL INICIALIZAR BASE DE DATOS",
      color: "red",
      break: true, //rompe regla de consola
      childre: function () {
        ConsoleInfo({
          text: "codigo: " + error.code,
          color: "yellow",
        });
        ConsoleInfo({
          text: "message: " + error.message,
          color: "yellow",
        });
      },
    });
  }
}

async function GetLanguage(id_language) {
  //Consulta mensajes de sistemas staticos.
  let name = "";
  switch (id_language) {
    case 1:
      name = "United_Kingdom";
      break;
    case 2:
      name = "Spain";
      break;
    default:
      console.log("language is not indexed");
      name = "United_Kingdom";
      break;
  }

  let objResponse = StaticLanguage[name];
  objResponse.name = name;
  return objResponse;
}



module.exports = {
  ConsoleTabInfo,
  ConsoleInfo,
  ConsoleInfoDebugger,
  InicializarBaseDatos,
  GetLanguage
};
