var nodemailer = require('nodemailer');


async function EnviarEmail(data) {
  let asunto = data.asunto;
  let para = data.para || [];
  let obj = { status: 403, msj: "Operación incorrecta." }

  let texto = data.texto;
  let html = data.html;
  let attachments = data.attachments;
  let conexion = data.conexion;
  var mailOptions = {
    from: conexion.usuario,//sender address
    to: para,//list of receivers
    subject: asunto,//Subject line
    text: texto,//plaintext body
    html: html,//html body
    attachments,//adjuntos
  };


  var transporter = nodemailer.createTransport({
    service: "gmail",
    host: 'smtp.gmail.com',
    port: 465,
    transporteMethod: 'SMTP',
    secure: true,
    auth: {
      user: conexion.usuario,
      pass: conexion.clave,
    }
  });
  return await transporter.sendMail(mailOptions, async function (error, response) {
    if (error) {
      obj.status = 403;
      obj.msj = error.message;
      obj.data = error;
      console.log(obj);
      return obj;
    } else {
      obj.status = 200;
      obj.msj = " Correo enviado exitosamente!!";
      obj.data = response;
      console.log(obj);
      return obj;
    }
  });


}

async function FormatoSaludoHTML(params) {

  let logo_empresa = "https://i.ibb.co/C7tYw9S/BANER-QUBE.png";
  let logo_facebook = "https://i.ibb.co/hWDx5pk/siguenos-facebook.png";
  // las imagenes logo_empresa  y  logo_facebook  estan alojadas en el servidor ImgBB
  let link_portal = params.link_portal;
  let link_facebook = params.link_facebook
  let empresa = params.empresa;
  let cliente = params.clie;
  let texto = params.texto;
  let HTML = `
  <div>
  <div
    style="display:none; font-size:1px; color:#333333; line-height:1px; max-height:0px; max-width:0px; opacity:0; overflow:hidden">
    Gracias por preferirnos
  </div>
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#f1f1f1">
    <tbody>
      <tr>
        <td align="center" valign="top" class="x_toppadding" style="padding:22px 0 40px 0">
          <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="x_main">
            <tbody>
              <tr>
                <td height="5" style="background-color:#F8963C"></td>
              </tr>
              <tr>
                <td align="left" valign="top">
                  <table width="600" border="0" align="center" cellpadding="0" cellspacing="0"
                    class="x_main" style="width:100%!important">
                    <tbody>
                      <tr>
                        <td align="center" valign="middle" bgcolor="#ffffff"
                          style="padding:10px 25px 10px 20px">
                          <table border="0" align="left" cellpadding="0" cellspacing="0"
                            width="100%">
                            <tbody>
                              <tr>
                                <td align="center" valign="top">
                                    <a href="${link_portal}"
                                    target="_blank">
                                      <img style= "display:block; margin:0px auto;width: 550;height: 100px;"
                                      data-imagetype="External"
                                      src="${logo_empresa}"
                                      alt="${empresa}" title="${empresa}"
                                      style="margin:0px auto;"
                                      class="CToWUd">
                                  </a>

                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <table border="0" align="right" cellpadding="0" cellspacing="0"
                            style="padding:20px 0 0 0">
                            <tbody>
                              <tr>
                                <td align="right"></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td align="center" valign="top"
                                                    style="border-bottom:1px solid #ececec!important">
                          <img data-imagetype="External"
                            src="https://static.locanto.com.ec/images/border.png" border="0"
                            class="x_border-img" width="600" height="17" alt=""
                            style="display:block; width:100%!important; height:1px">
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table width="600" border="0" align="center" cellpadding="0" cellspacing="0"
                    class="x_main">
                    <tbody>
                      <tr>
                        <td align="left" valign="top" style="background:#ffffff; padding:0px">
                          <table class="x_sidepadding" border="0" align="center"
                            cellpadding="0" cellspacing="0"
                            style="background-color:#FFFFFF; border-bottom:1px solid #eeeeee; border-top:5px solid #fbfbfb; width:100%">
                            <tbody>
                              <tr>
                                <td>
                                  <table cellpadding="0" cellspacing="0"
                                    align="center">
                                    <tbody>
                                      <tr>
                                        <td
                                          style="font-size:22px; line-height:100%; font-weight:normal; color:#333; padding-bottom:0px; padding-top:18px; text-align:center">
                                          Estimad@ ${cliente}
                                          </td>
                                      </tr>
                                      <tr>
                                        <td
                                          style="font-size:14px!important; padding-bottom:20px; padding-top:10px; text-align:center; max-width:500px!important">
                                          ${texto}
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <table class="x_sidepadding" border="0" align="center"
                            cellpadding="0" cellspacing="0"
                            style="background-color:#325884; border-bottom:1px solid #cccccc; width:100%; min-width:325px; color:#fff; padding-bottom:20px">
                            <tbody>
                              <tr>
                                <td align="center"
                                  style="font-size:14px; padding-top:26px; padding-left:10px; padding-right:10px">
                                  Para mas información ingrese a nuestro portal aquí:
                                </td>
                              </tr>
                              <tr>
                                <td align="center">
                                  <table align="center" width="200"
                                    style="border-bottom:3px solid #1d326f; border-right:3px solid #1d326f; width:272px; background-color:#fff; border-radius:10px; margin-top:20px; margin-bottom:10px; width:auto">
                                    <tbody>
                                      <tr>
                                        <td align="center"
                                          style="padding:5px 10px">
                                          <a href="${link_portal}"
                                            target="_blank"
                                            rel="noopener noreferrer"
                                            data-auth="NotApplicable"
                                            style="text-decoration:none; color:#325884; font-weight:bold; font-size:16px">PORTAL AQUÍ</a>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table> 
                        </td>
                      </tr>                    
                      <tr>
                        <td align="center" valign="top" style="background:#ffffff">
                          <table width="100%" border="0" align="center" cellpadding="0"
                            cellspacing="0" class="x_main" style="width:100%!important">
                            <tbody>
                              <tr>
                                <td
                                  style="padding:20px;color:#8b8b8b;text-align:center">
                                  <a href="${link_facebook}"
                                    target="_blank">
                                                                        <img style= "display:block; margin:0px auto;width: 550;height: 100px;"
                                                                        data-imagetype="External" src= "${logo_facebook}"
                                                                        alt="Facebook" title="Facebook"
                                      style="width: 200px;margin:0px auto;"
                                      class="CToWUd">
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <td align="center" valign="top"
                                  style="color:#777; background:#fff; padding:10px 8px 10px 13px">
                                  No respondas a este
                                  correo, ya que no se
                                  encuentra habilitado
                                  para recibir mensajes.
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <table width="100%" border="0" align="center" cellpadding="0"
                            cellspacing="0">
                            <tbody>
                              <tr>
                                <td align="center" valign="top"
                                  style="font-size:11px; color:#777!important">
                                  Copyright © 2022  "${empresa}™. Todos los derechos reservados.
                                </td>
                              </tr>
                            </tbody>
                          </table>

                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
  </div>
  `
  return HTML;
}
module.exports = {
  EnviarEmail,
  FormatoSaludoHTML
}
