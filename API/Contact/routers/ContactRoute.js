const express = require("express");
const router = express.Router();

const Controller = require("../controllers/ContactController");
router.route("/contact/save").post(Controller.ContactSave);

module.exports = router;
