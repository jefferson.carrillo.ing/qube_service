const Utilitys = app.pro.Utilitys;
const configure_exe = app.pro.configure_exe;
const config = app.pro.config;
const ConnectionBD = require("../../../config/ConnectionPG");
const ResponseModel = require("../../../Model/ResponseModel");
const ContactModel = require("../models/ContactModel");
const EmailController = require("../../../utils/Email");


async function ContactSave(req, res, next) {
  let objResponse = new ResponseModel();
  try {
    let p = req.body;
    let email = p.email;
    let description = p.description;
    let id_language = p.id_language;

    let objInsert = new ContactModel();
    objInsert.email = email;
    objInsert.description = description;
    objInsert.id_language = id_language;
    objInsert.condition = 1;

    let objContact = await ConnectionBD.knex("contact")
      .returning("*")
      .insert(objInsert);

    let objLanguage = await Utilitys.GetLanguage(id_language);

    if (objContact.length == 1) {
      let message = objLanguage.contact.received;
      objResponse.status = 201;
      objResponse.message = message;
      objContact = objContact[0];
      objResponse.data = { objContact };
      sendEmailResponse(objContact);
    } else {
      let message = objLanguage.global.error;
      objResponse.status = 403;
      objResponse.message = message;
    }
  } catch (error) {
    objResponse.status = 500;
    objResponse.message = error.message;
  }

  res.status(objResponse.status).jsonp(objResponse);
}

async function sendEmailResponse(objContact) {

  let visit_email = objContact.email;
  let visit_mensaje = objContact.description;
  let operator_email = ["jaccarrillo@outlook.es", "fquinteros@qubebeach.com"];
  let empresa = config.nombre;
  let link_portal = configure_exe.link_frontend;
  let link_facebook = "";

  let visit_texto = "Hemos receptado su mensaje <b>" + visit_mensaje + "</b>, pronto un accesor se comunicara con usted.";
  let visit_html = await EmailController.FormatoSaludoHTML({ link_portal, link_facebook, empresa, clie: "Visitante", texto: visit_texto });
  await EmailController.EnviarEmail({
    asunto: "QUBE",
    para: [visit_email],
    html: visit_html,
    attachments: [],
    conexion: {
      usuario: configure_exe.email.user,
      clave: configure_exe.email.pass
    }
  });


  let operator_texto = "Hemos el asesor el visitante " + visit_email + "dejo el siguiente mensaje  <b>" + visit_mensaje + "</b>, requiere mas informacion contactese con el.";
  let operator_html = await EmailController.FormatoSaludoHTML({ link_portal, link_facebook, empresa, clie: "Asesor", texto: operator_texto });
  await EmailController.EnviarEmail({
    asunto: "QUBE",
    para: operator_email,
    html: operator_html,
    attachments: [],
    conexion: {
      usuario: configure_exe.email.user,
      clave: configure_exe.email.pass
    }
  });
}

module.exports = { ContactSave };
