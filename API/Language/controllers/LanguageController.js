const Utilitys = app.pro.Utilitys;
const ConnectionBD = require("../../../config/ConnectionPG");
const ResponseModel = require("../../../Model/ResponseModel");
const LanguageModel = require("../models/LanguageModel");

async function LanguageSave(req, res, next) {
  let objResponse = new ResponseModel();
  try {
    let p = req.body;
    let name = p.name;
    let value = p.value;
    let id_language = p.id_language;
    let condition = p.condition;

    let objInsert = new LanguageModel();
    objInsert.name = name;
    objInsert.value = value;
    objInsert.condition = condition;

    let objLanguage = await ConnectionBD.knex("language")
      .returning("*")
      .insert(objInsert);

    let objLanguage2 = await Utilitys.GetLanguage(id_language);

    if (objLanguage.length == 1) {
      let message = objLanguage2.language.save;
      objResponse.status = 201;
      objResponse.message = message;
      objLanguage = objLanguage[0];
      objResponse.data = { objLanguage };
    } else {
      let message = objLanguage2.global.error;
      objResponse.status = 403;
      objResponse.message = message;
    }
  } catch (error) {
    objResponse.status = 500;
    objResponse.message = error.message;
  }

  res.status(objResponse.status).jsonp(objResponse);
}


async function LanguageShowList(req, res, next) {
  let objResponse = new ResponseModel();
  try {
    let p = req.body;
    let id_language = p.id_language;
    let select = ["id", "name", "value", "img"];
    let objLanguage = await ConnectionBD.knex("language").select(select).where({ condition: 1 });
    let objLanguage2 = await Utilitys.GetLanguage(id_language);

    if (objLanguage.length != null) {
      let message = objLanguage2.global.received;
      objResponse.status = 200;
      objResponse.message = message;
      objResponse.data = { objLanguage };
    } else {
      let message = objLanguage2.global.error;
      objResponse.status = 204;
      objResponse.message = message;
    }
  } catch (error) {
    objResponse.status = 500;
    objResponse.message = error.message;
  }

  res.status(objResponse.status).jsonp(objResponse);
}

module.exports = { LanguageSave, LanguageShowList };
