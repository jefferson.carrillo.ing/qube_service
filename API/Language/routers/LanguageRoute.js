const express = require("express");
const router = express.Router();

const Controller = require("../controllers/LanguageController");
router.route("/language/save").post(Controller.LanguageSave);
router.route("/language/show").post(Controller.LanguageShowList);

module.exports = router;
