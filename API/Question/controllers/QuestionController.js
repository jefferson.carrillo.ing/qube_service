const Utilitys = app.pro.Utilitys;
const ConnectionBD = require("../../../config/ConnectionPG");
const ResponseModel = require("../../../Model/ResponseModel");
const QuestionModel = require("../models/QuestionModel");

async function QuestionSave(req, res, next) {
  let objResponse = new ResponseModel();
  try {
    let p = req.body;
    let description = p.description;
    let id_language = p.id_language;

    let objInsert = new QuestionModel();
    objInsert.description = description;
    objInsert.id_language = id_language;
    objInsert.condition = 1;


    let objQuestion = await ConnectionBD.knex("questions")
      .returning("*")
      .insert(objInsert);

    let objLanguage = await Utilitys.GetLanguage(id_language);

    if (objQuestion.length == 1) {
      let message = objLanguage.question.received;
      objResponse.status = 200;
      objResponse.message = message;
      objQuestion = objQuestion[0];
      objResponse.data = { objQuestion };
    } else {
      let message = objLanguage.global.error;
      objResponse.status = 403;
      objResponse.message = message;
    }
  } catch (error) {
    objResponse.status = 500;
    objResponse.message = error.message;
  }

  res.status(objResponse.status).jsonp(objResponse);
}


module.exports = { QuestionSave };
