const express = require("express");
const router = express.Router();

const Controller = require("../controllers/QuestionController");
router.route("/question/save").post(Controller.QuestionSave);

module.exports = router;
