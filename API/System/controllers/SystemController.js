const Utilitys = app.pro.Utilitys;
const ConnectionBD = require("../../../config/ConnectionPG");
const ResponseModel = require("../../../Model/ResponseModel");
const SystemModel = require("../models/SystemModel");

async function SystemSave(req, res, next) {
  let objResponse = new ResponseModel();
  try {
    let p = req.body;
    let key = p.key;
    let value = p.value;
    let description = p.description;
    let id_language = p.id_language;
    let type = p.type;

    let objInsert = new SystemModel();
    objInsert.key = key;
    objInsert.value = value;
    objInsert.description = description;
    objInsert.id_language = id_language;
    objInsert.type = type;
    objInsert.condition = 1;

    let objSystem = await ConnectionBD.knex("system")
      .returning("*")
      .insert(objInsert);

    let objLanguage = await Utilitys.GetLanguage(id_language);


    if (objSystem.length == 1) {
      let message = objLanguage.system.save;
      objResponse.status = 201;
      objResponse.message = message;
      objSystem = objSystem[0];
      objResponse.data = { objSystem };
    } else {
      let message = objLanguage.global.error;
      objResponse.status = 403;
      objResponse.message = message;
    }
  } catch (error) {
    objResponse.status = 500;
    objResponse.message = error.message;
  }

  res.status(objResponse.status).jsonp(objResponse);
}

async function SystemUpdate(req, res, next) {
  let objResponse = new ResponseModel();
  try {
    let p = req.body;
    let id = p.id;
    let key = p.key;
    let value = p.value;
    let description = p.description;
    let id_language = p.id_language;
    let type = p.type;
    let condition = p.condition;

    let objUpdate = new SystemModel();
    if (key) {
      objUpdate.key = key;
    }
    if (value) {
      objUpdate.value = value;
    }
    if (description) {
      objUpdate.description = description;
    }
    if (id_language) {
      objUpdate.id_language = id_language;
    }
    if (type) {
      objUpdate.type = type;
    }

    objUpdate.condition = condition || 1;

    let objSystem = await ConnectionBD.knex("system")
      .returning("*")
      .where({ id })
      .update(objUpdate);

    let objLanguage = await Utilitys.GetLanguage(id_language);


    if (objSystem.length == 1) {
      let message = objLanguage.system.update;
      objResponse.status = 201;
      objResponse.message = message;
      objSystem = objSystem[0];
      objResponse.data = { objSystem };
    } else {
      let message = objLanguage.global.error;
      objResponse.status = 403;
      objResponse.message = message;
    }
  } catch (error) {
    objResponse.status = 500;
    objResponse.message = error.message;
  }

  res.status(objResponse.status).jsonp(objResponse);
}
async function SystemShow(req, res, next) {
  let objResponse = new ResponseModel();
  try {
    let p = req.body;
    let filter = p.filter;
    let id_language = p.id_language;
    let select = [
      "id",
      "key",
      "value",
      "description",
      "type",
      "id_language"
    ];
    let objSystem = await ConnectionBD.knex("system").select(select)
      .andWhere(function () {
        this.whereIn('id_language', [0, id_language]);
        this.where(filter);
      });

    let objLanguage = await Utilitys.GetLanguage(id_language);

    if (objSystem.length == 0) {
      let message = objLanguage.global.not_content;
      objResponse.status = 201;
      objResponse.message = message;
    } else if (objSystem.length != null) {
      let message = objLanguage.global.received;
      objResponse.status = 200;
      objResponse.message = message;
      objResponse.data = { objSystem };
    } else {
      let message = objLanguage.global.error;
      objResponse.status = 204;
      objResponse.message = message;
    }
  } catch (error) {
    objResponse.status = 500;
    objResponse.message = error.message;
  }

  res.status(objResponse.status).jsonp(objResponse);
}
module.exports = { SystemSave, SystemUpdate, SystemShow };
