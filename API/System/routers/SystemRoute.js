const express = require('express');
const router = express.Router(); 

const Controller = require("../controllers/SystemController");
router.route("/system/save").post(Controller.SystemSave);
router.route("/system/update").post(Controller.SystemUpdate);
router.route("/system/show").post(Controller.SystemShow);

module.exports = router;
