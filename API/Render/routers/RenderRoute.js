const express = require('express');
const router = express.Router(); 

const Controller = require("../controllers/RenderController");
router.route("/render/save").post(Controller.RenderSave);
router.route("/render/update").post(Controller.RenderUpdate);
router.route("/render/show").post(Controller.RenderShow);

module.exports = router;
