class RenderModel {
  key = null;
  title = null;
  icon = null;
  description = null;
  id_language = null;
  condition = null;
  type = null;
  content = null;

}

module.exports = RenderModel;
