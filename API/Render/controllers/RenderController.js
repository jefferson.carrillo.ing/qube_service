const Utilitys = app.pro.Utilitys;
const ConnectionBD = require("../../../config/ConnectionPG");
const ResponseModel = require("../../../Model/ResponseModel");
const RenderModel = require("../models/RenderModel");

async function RenderSave(req, res, next) {
  let objResponse = new ResponseModel();
  try {
    let p = req.body;
    let key = p.key;
    let title = p.title;
    let icon = p.icon;
    let description = p.description;
    let id_language = p.id_language;
    let type = p.type;
    let content = p.content;

    let objInsert = new RenderModel();
    objInsert.key = key;
    objInsert.title = title;
    objInsert.icon = icon;
    objInsert.description = description;
    objInsert.id_language = id_language;
    objInsert.type = type;
    objInsert.content = content;
    objInsert.condition = 1;

    let objRender = await ConnectionBD.knex("render")
      .returning("*")
      .insert(objInsert);

    let objLanguage = await Utilitys.GetLanguage(id_language);


    if (objRender.length == 1) {
      let message = objLanguage.render.save;
      objResponse.status = 201;
      objResponse.message = message;
      objRender = objRender[0];
      objResponse.data = { objRender };
    } else {
      let message = objLanguage.global.error;
      objResponse.status = 403;
      objResponse.message = message;
    }
  } catch (error) {
    objResponse.status = 500;
    objResponse.message = error.message;
  }

  res.status(objResponse.status).jsonp(objResponse);
}

async function RenderUpdate(req, res, next) {
  let objResponse = new ResponseModel();
  try {
    let p = req.body;
    let id = p.id;
    let key = p.key;
    let title = p.title;
    let icon = p.icon;
    let description = p.description;
    let id_language = p.id_language;
    let type = p.type;
    let content = p.content;
    let condition = p.condition;

    let objUpdate = new RenderModel();
    if (key) {
      objUpdate.key = key;
    }
    if (title) {
      objUpdate.title = title;
    }
    if (icon) {
      objUpdate.icon = icon;
    }
    if (description) {
      objUpdate.description = description;
    }
    if (id_language) {
      objUpdate.id_language = id_language;
    }
    if (type) {
      objUpdate.type = type;
    }
    if (content) {
      objUpdate.content = content;
    }
    objUpdate.condition = condition || 1;

    let objRender = await ConnectionBD.knex("render")
      .returning("*")
      .where({ id })
      .update(objUpdate);

    let objLanguage = await Utilitys.GetLanguage(id_language);


    if (objRender.length == 1) {
      let message = objLanguage.render.update;
      objResponse.status = 201;
      objResponse.message = message;
      objRender = objRender[0];
      objResponse.data = { objRender };
    } else {
      let message = objLanguage.global.error;
      objResponse.status = 403;
      objResponse.message = message;
    }
  } catch (error) {
    objResponse.status = 500;
    objResponse.message = error.message;
  }

  res.status(objResponse.status).jsonp(objResponse);
}
async function RenderShow(req, res, next) {
  let objResponse = new ResponseModel();
  try {
    let p = req.body;
    let filter = p.filter;
    let id_language = p.id_language;
    let objRender = await ConnectionBD.knex("render").select("*").andWhere(filter);
    let objLanguage = await Utilitys.GetLanguage(id_language);

    if (objRender.length == 0) {
      let message = objLanguage.global.not_content;
      objResponse.status = 201;
      objResponse.message = message;
      let key = filter.key;
      objRender = {};
      objRender.id = 0;
      objRender.key = key;
      objRender.icon = "fa fa-ban";
      objRender.title = 'No se encontro data en <code>' + key + '</code>';
      objRender.content = message;
      objResponse.data = { objRender };
    } else if (objRender.length != null) {
      let message = objLanguage.global.received;
      objResponse.status = 200;
      objResponse.message = message;
      objRender = objRender[0];
      objResponse.data = { objRender };
    } else {
      let message = objLanguage.global.error;
      objResponse.status = 204;
      objResponse.message = message;
    }
  } catch (error) {
    objResponse.status = 500;
    objResponse.message = error.message;
  }

  res.status(objResponse.status).jsonp(objResponse);
}
module.exports = { RenderSave, RenderUpdate, RenderShow };
